package router

import (
	"math/rand"
	"testing"
)

var benchResponse Response

func benchmarkMatch(method string, path string, b *testing.B) {
	b.StopTimer()
	var res Response
	r := MockRouterWithRoutes(createRoutes(1000))
	b.ResetTimer()
	b.StartTimer()
	for n := 0; n < b.N; n++ {
		res = r.Match(method, path)
	}
	benchResponse = res
}

func Benchmark_MatchRoute1(b *testing.B) { benchmarkMatch("GET", "/", b) }
func Benchmark_MatchRoute2(b *testing.B) { benchmarkMatch("GET", "/home-pre1/about-pre1/10/10", b) }

func Benchmark_MatchRoute3(b *testing.B) { benchmarkMatch("GET", "/unknown", b) }

//MockRouterWithRoutes Create router with paths.
func MockRouterWithRoutes(mp map[string]string) Router {
	r := NewRouter("", NewDefaultOptions())
	for path, method := range mp {
		r.Add(method, path)
	}
	return r
}

func createRoutes(c int) map[string]string {
	getRand := func(s []string) string {
		n := rand.Int() % len(s)
		return s[n]
	}
	routes := make(map[string]string)

	directories := []string{
		"/home", "/about", "/foo", "/bar", "/bat", "/baz", "/lorem", "/ipsum", "/et", "/domo",
	}

	prefix := []string{
		"-pre1", "-pre2", "-pre3", "-pre4", "-pre5", "-pre6",
	}

	vars := []string{
		"/:(0-9)-var1", "/:(a-z)-var2", "/:(A-Za-z0-0)-var3", "/:(A-z0-9)-var4", "/:(A-Z)-var5", "/:(1-4)-var6",
	}

	for i := 0; i < c; i++ {
		path := getRand(directories) + getRand(prefix) + getRand(prefix)
		routes[path] = "GET"
	}
	for i := 0; i < c; i++ {
		path := getRand(directories) + getRand(prefix) + getRand(vars)
		routes[path] = "GET"
	}
	for i := 0; i < c; i++ {
		path := getRand(directories) + getRand(prefix) + getRand(directories) + getRand(prefix)
		routes[path] = "GET"
	}
	for i := 0; i < c; i++ {
		path := getRand(directories) + getRand(prefix) + getRand(directories) + getRand(prefix) + getRand(vars)
		routes[path] = "GET"
	}
	for i := 0; i < c; i++ {
		path := getRand(directories) + getRand(prefix) + getRand(directories) + getRand(prefix) + getRand(directories) + getRand(prefix)
		routes[path] = "GET"
	}
	for i := 0; i < c; i++ {
		path := getRand(directories) + getRand(prefix) + getRand(directories) + getRand(prefix) + getRand(directories) + getRand(prefix) + getRand(vars)
		routes[path] = "GET"
	}

	//add some known routes for benchmarks
	//first route
	routes["/"] = "GET"
	routes["/home-pre1/about-pre1/:(0-9)-var2/:(0-9)-var3"] = "GET"
	routes["/lorem-pre1/:(0-9)-var1/:(0-9)-var2/:(0-9)-var3"] = "GET"
	//last route
	routes["/z-last/zz/:(0-9)-var1/:(0-9)-var2"] = "GET"
	return routes
}
func Benchmark_MatchRoute4(b *testing.B) { benchmarkMatch("GET", "/lorem-pre1/12/12/1", b) }
