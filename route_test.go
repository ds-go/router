package router

import (
	"crypto/md5"
	"encoding/hex"
	"errors"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestGetMethod(t *testing.T) {
	expected := "GET"
	r := NewRoute(expected, "/path", nil, nil, "")
	assert.Equal(t, expected, r.GetMethod())
}

func Test_newErrorParam(t *testing.T) {
	expected := &param{
		g: "error",
		s: "",
		r: "",
		p: "",
		n: "",
	}
	expectedErr := "error message"
	actual, actualerr := _newErrorParam(expectedErr)
	assert.Equal(t, expected, actual)
	assert.Equal(t, errors.New(expectedErr), actualerr)
}

func TestGetPathRoute(t *testing.T) {
	expected := "/path"
	r := NewRoute("GET", expected, nil, nil, "")
	assert.Equal(t, expected, r.GetPath())
}

func TestGetIDRoute(t *testing.T) {
	expected := "ID"
	r := NewRoute("GET", "/path", nil, nil, expected)
	assert.Equal(t, expected, r.GetID())
}

func TestGetParams(t *testing.T) {
	expected := make([]RouteParam, 0)
	r := NewRoute("GET", "/path", expected, nil, "")
	assert.Equal(t, expected, r.GetParams())
}

func TestGetAliases(t *testing.T) {
	expected := make([]RouteAlias, 0)
	r := NewRoute("GET", "/path", nil, expected, "")
	assert.Equal(t, expected, r.GetAliases())
}

func TestCreateID(t *testing.T) {}

func Test_hashString(t *testing.T) {
	s := "some-string"
	hasher := md5.New()
	hasher.Write([]byte(s))
	expected := hex.EncodeToString(hasher.Sum(nil))
	actual, _ := _hashString(s)
	assert.Equal(t, expected, actual)
}

/**
End of Tests.
Start of Benchmarks
**/
func benchmarkhashString(s string, b *testing.B) {
	for n := 0; n < b.N; n++ {
		_hashString(s)
	}
}
func Benchmark_hashString1(b *testing.B) { benchmarkhashString("a", b) }
func Benchmark_hashString2(b *testing.B) { benchmarkhashString("01234567890", b) }
func Benchmark_hashString3(b *testing.B) { benchmarkhashString("foobar", b) }
func Benchmark_hashString4(b *testing.B) { benchmarkhashString("the-brown-fox-jumped-over", b) }
