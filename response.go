package router

// Response is returned from a Router::matchRoute
// A Response from the router only contains the underlying route information and url variables
// This needs to be upgraded to a Dispatcher.Response in order to contaner handler information
// @see gitlab.com/krobolt/go-distpatcher
type Response interface {
	GetRoute() Route
	GetStatusCode() int
	GetVars() map[string]interface{}
}

type routerResponse struct {
	status int
	route  Route
	v      map[string]interface{}
}

//NewResponse returns a new Response
func NewResponse(status int, r Route, vars map[string]interface{}) Response {
	return &routerResponse{
		status: status,
		route:  r,
		v:      vars,
	}
}

func (r *routerResponse) GetRoute() Route {
	return r.route
}

func (r *routerResponse) GetStatusCode() int {
	return r.status
}

func (r *routerResponse) GetVars() map[string]interface{} {
	return r.v
}
