package router

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestNewRouter(t *testing.T) {
	expected := createMockRouter()
	actual := NewRouter("/", NewDefaultOptions())
	assert.Equal(t, expected, actual)
}

func TestNewDefaultOptions(t *testing.T) {
	vs := `:`
	open := `-(`
	close := `)`
	p := `a-zAZ0-9.\-_`
	vsr := vs + `\(([` + p + `+]+)\)` + open + `[` + p + `]+` + close
	expected := Options{
		VarSep:          vs,
		OptionalSep:     `|`,
		AllowedChars:    p,
		AllowedVarChars: p,
		Pattern:         vsr,
		Separator:       "/",
	}
	actual := NewDefaultOptions()
	assert.Equal(t, expected, actual)
}
func TestGetOptions(t *testing.T) {
	r := NewRouter("/", NewDefaultOptions())
	o := NewDefaultOptions()
	assert.Equal(t, r.GetOptions(), o)
}
func Test_Group(t *testing.T) {
	r := NewRouter("/base", NewDefaultOptions())
	r.Group("/new/root", func() {
		expected := "/base/new/root"
		actual := r.GetBase()
		assert.Equal(t, expected, actual)
	})
}
func TestGetBase(t *testing.T) {
	expected := "/base/new/root"
	r := NewRouter(expected, NewDefaultOptions())
	actual := r.GetBase()
	assert.Equal(t, expected, actual)
}
func Test_getParamsFromPath(t *testing.T) {
	p1 := NewParam("base", "foobar", "foobar", "foobar", "", 0)
	p2 := NewParam("req", ":(a-z)-foo", "[a-z]+", "foo", "foo", 2)
	path := "/foobar/:(a-z)-foo"
	r := createMockRouter()

	expected := []RouteParam{
		p1,
		p2,
	}
	actual, e := r._getParamsFromPath(path)
	assert.NoError(t, e)
	assert.Equal(t, expected, actual)
}

func TestMatchRoute(t *testing.T) {
	r := NewRouter("", NewDefaultOptions())

	expectedVars := make(map[string]interface{})
	expectedVars["foo"] = "variable"

	expectedStatus := 2

	r.Add("POST", "/another/path/:(a-z)-foo")

	expectedRoute, _ := r.Add("GET", "/another/path/:(a-z)-foo")

	expectedResponse := NewResponse(expectedStatus, expectedRoute, expectedVars)

	actual := r.Match("GET", "/another/path/variable")

	assert.Equal(t, expectedResponse, actual)
}
func TestMatchRouteMulti(t *testing.T) {
	r := NewRouter("", NewDefaultOptions())
	expectedVars := make(map[string]interface{})
	expectedVars["foo"] = "variable"
	expectedVars["bar"] = "1234"
	expectedStatus := 2
	expectedRoute, _ := r.Add("GET", "/another/path/:(a-z)-foo/:(0-9)-bar")
	expectedResponse := NewResponse(expectedStatus, expectedRoute, expectedVars)
	actual := r.Match("GET", "/another/path/variable/1234")
	assert.Equal(t, expectedResponse, actual)
}

func TestMatchRouteMultiPartial(t *testing.T) {
	r := NewRouter("", NewDefaultOptions())
	expectedVars := make(map[string]interface{})
	expectedVars["foo"] = "variable"
	expectedVars["bar"] = "1234"
	expectedVars["baz"] = "1234"
	expectedStatus := 2
	expectedRoute, _ := r.Add("GET", "/another/path/partial-!:(a-z)-foo/another-mixed-uri-!:(0-9)-bar/:(0-9)-baz!-baz")
	expectedResponse := NewResponse(expectedStatus, expectedRoute, expectedVars)
	actual := r.Match("GET", "/another/path/partial-variable/another-mixed-uri-1234/1234-baz")
	assert.Equal(t, expectedResponse, actual)
}

func TestMatch(t *testing.T) {
	r := NewRouter("", NewDefaultOptions())
	r.Add("GET", "/some/path")
	rt, _ := r.Add("GET", "/another/path")
	id, actual := r.Find("GET", "/another/path")
	expected := 2
	assert.Equal(t, expected, actual)
	assert.Equal(t, rt.GetID(), id)
}

func TestMatchWrongMethod(t *testing.T) {
	r := NewRouter("", NewDefaultOptions())
	r.Add("POST", "/another/path")
	_, actual := r.Find("GET", "/another/path")
	expected := 1
	assert.Equal(t, expected, actual)
}

func Test_getParamsFromPathMultibase(t *testing.T) {
	p1 := NewParam("base", "foobar/bar", "foobar/bar", "foobar/bar", "", 0)
	p2 := NewParam("req", ":(a-z)-foo", "[a-z]+", "foo", "foo", 3)
	path := "/foobar/bar/:(a-z)-foo"
	r := createMockRouter()
	expected := []RouteParam{
		p1,
		p2,
	}
	actual, e := r._getParamsFromPath(path)
	assert.NoError(t, e)
	assert.Equal(t, expected, actual)
}
func Test_prependParam(t *testing.T) {

	p1 := NewParam("", "s2", "g1", "g1", "g1", 0)
	p2 := NewParam("", "s2", "g2", "g2", "g2", 1)
	p3 := NewParam("", "s3", "g3", "g3", "g3", 2)

	params := make([]RouteParam, 0)
	actual := append(params, p3, p1, p2)
	expected := append(params, p1, p2)

	expected = _prependParam(p3, expected)
	assert.Equal(t, expected, actual)
}

func Test_matchRegex(t *testing.T) {
	expected := true
	provider := [][]string{
		[]string{"/foo", "/foo"},
		[]string{"/[a-z]+", "/abcdefghijklmnopqrstuvwxyz"},
		[]string{"/[0-9]+", "/0123456789"},
	}
	for _, v := range provider {
		actual := _matchRegex(v[0], v[1])
		assert.Equal(t, expected, actual)
	}
}
func Test_matchRegexFail(t *testing.T) {
	expected := false
	provider := [][]string{
		[]string{"/foob", "/foo"},
		[]string{"/[A-Z]+", "/abcdefghijklmnopqrstuvwxyz"},
		[]string{"/[1-5]+", "/678900"},
	}
	for _, v := range provider {
		actual := _matchRegex(v[0], v[1])
		assert.Equal(t, expected, actual)
	}
}
func Test__createParamBase(t *testing.T) {
	group := "base"
	source := "somepath/foo"
	r := createMockRouter()
	actual, _ := CreateParamFromString(r.options.Pattern, r.options.Separator, group, source, 0)
	expected := NewParam(group, source, source, source, "", 0)
	assert.Equal(t, expected, actual)
}
func Test__createParamRequired(t *testing.T) {
	group := "req"
	source := ":(a-z)-var"
	name := "var"
	r := createMockRouter()
	actual, _ := CreateParamFromString(r.options.Pattern, r.options.Separator, group, source, 0)
	expected := NewParam(group, source, "[a-z]+", name, name, 0)
	assert.Equal(t, expected, actual)
}
func Test__createParamOptional(t *testing.T) {
	group := "opt"
	name := "rndname"
	source := "|/:(a-z)-" + name
	r := createMockRouter()
	actual, _ := CreateParamFromString(r.options.Pattern, r.options.Separator, group, source, 0)
	expected := NewParam(group, source, "[a-z]+", name, name, 0)
	assert.Equal(t, expected, actual)
}
func Test_isValid(t *testing.T) {
	path := "/foobar/(a-z)-foo"
	params := make([]RouteParam, 0)
	p1 := NewParam("base", "foobar", "foobar", "foobar", "", 0)
	p2 := NewParam("req", "(a-z)-foo", "[a-z]+", "foo", "foo", 0)
	params = append(params, p1)
	params = append(params, p2)
	expected := true
	actual := _isValid(path, params)
	assert.Equal(t, expected, actual)
}
func Test_split(t *testing.T) {
	var (
		expected []string
	)
	dataIn := [][]string{
		[]string{"/", "/some/path"},
		[]string{":", ":some:path:"},
		[]string{"0", "0some0path"},
	}
	dataOut := [][]string{
		[]string{"some", "path"},
		[]string{"some", "path"},
		[]string{"some", "path"},
	}
	for i, v := range dataIn {
		ok, actual := _split(v[0], v[1])
		expected = dataOut[i]
		assert.Equal(t, expected, actual)
		assert.Equal(t, true, ok)
	}
}
func Test_splitSingleParam(t *testing.T) {
	var (
		expected []string
	)
	dataIn := [][]string{
		[]string{"/", "/somepath"},
		[]string{":", ":anotherpath"},
	}
	dataOut := [][]string{
		[]string{"somepath"},
		[]string{"anotherpath"},
	}
	for i, v := range dataIn {
		ok, actual := _split(v[0], v[1])
		expected = dataOut[i]
		assert.Equal(t, expected, actual)
		assert.Equal(t, false, ok)
	}
}

func Test_splitMultParam(t *testing.T) {
	var (
		expected []string
	)
	dataIn := [][]string{
		[]string{"/", "/somepath/foo"},
		[]string{"/", "/another/path/page"},
	}
	dataOut := [][]string{
		[]string{"somepath", "foo"},
		[]string{"another", "path", "page"},
	}
	for i, v := range dataIn {
		ok, actual := _split(v[0], v[1])
		expected = dataOut[i]
		assert.Equal(t, expected, actual)
		assert.Equal(t, true, ok)
	}
}

func createMockRouter() *router {
	return &router{
		options:  NewDefaultOptions(),
		routes:   make(map[string]Route),
		alias:    make(map[int][]RouteAlias),
		basepath: "/",
	}
}
