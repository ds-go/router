package router

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

var (
	group  = "base"
	source = "some-path"
	reg    = "some-path"
	path   = "some-path"
	name   = ""
)

func getParam() RouteParam {
	return NewParam(group, source, reg, path, name, 0)
}
func TestNewParam(t *testing.T) {

	expected := &param{
		g: group,
		s: source,
		r: reg,
		p: path,
		n: name,
	}

	assert.Equal(t, expected, NewParam(group, source, reg, path, name, 0))
}

func TestGetGroup(t *testing.T) {
	assert.Equal(t, group, getParam().GetGroup())
}

func TestGetName(t *testing.T) {
	assert.Equal(t, name, getParam().GetName())
}

func TestGetRegex(t *testing.T) {
	assert.Equal(t, reg, getParam().GetRegex())
}

func TestGetSource(t *testing.T) {
	assert.Equal(t, source, getParam().GetSource())
}

func TestGetPath(t *testing.T) {
	assert.Equal(t, path, getParam().GetPath())
}
